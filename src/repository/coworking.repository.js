'use strict'

const turf = require('@turf/turf')

module.exports = class CoworkingRepository {
  constructor(dynamoDb, dynamoDbTable){
    if(!dynamoDb || !dynamoDbTable){
      throw new Error('The instance dynamoDB is invalid')
    }
    this.dynamoDb = dynamoDb
    this.dynamoDbTable = dynamoDbTable
  }

  async save (coworking){
    const params = {
      TableName: this.dynamoDbTable,
      Item: {
        id: coworking.id,
        name: coworking.name,
        address: coworking.address, 
        country: coworking.country, 
        city: coworking.city, 
        state: coworking.state, 
        zipcode: coworking.zipcode, 
        lat: coworking.lat, 
        lng: coworking.lng
      }
    }

    const result = await this.dynamoDb.put(params).promise()

    return result
  }

  async getAll (){
    const params = {
      TableName: this.dynamoDbTable
    }
    const data = await this.dynamoDb.scan(params).promise()
    return data.Items
  }

  async getCoworkingMinDistance (myLocation) {
    const result = await this.getAll()

    let minDist = Number.POSITIVE_INFINITY
    let coworking = null
    const location = turf.point([myLocation.lat, myLocation.lng]);
    const options = {units: 'kilometers'};

    result.forEach(c => {
      var point = turf.point([c.lat, c.lng]);

      var distance = turf.distance(location, point, options);

      if(minDist > distance){
        minDist = distance
        coworking = c
      }
    })

    return coworking
  }
}