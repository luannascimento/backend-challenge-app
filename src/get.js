'use strict'

const validBody = require('./valid.body')
const AWS = require('aws-sdk')
const CoworkingRepository = require('./repository/coworking.repository')

const required = [['myLocation'], ['lat', 'lng']]

module.exports.get = async (event, context, callback) => {
  try {
    const data = JSON.parse(event.body)
    console.log(data)
    if(!validBody(required[0], data) || !validBody(required[1], data.myLocation)){
      callback(null, {
        statusCode: 400,
        body: JSON.stringify({
          message: `Bad Request Error`
        })
      })
    }

    const cr = new CoworkingRepository(new AWS.DynamoDB.DocumentClient(), process.env.DYNAMODB_TABLE)
    
    let coworking = await cr.getCoworkingMinDistance(data.myLocation)

    if(coworking)
      delete coworking.id

    callback(null, {
      statusCode: 200,
      body: JSON.stringify(coworking)
    })
  } catch (error) {
    callback(null, {
      statusCode: 500,
      body: JSON.stringify({
        message: error.message
      })
    })
  }
}