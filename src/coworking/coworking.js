'use strict'

module.exports = class Coworking {
  constructor(
    id, 
    name, 
    address, 
    country, 
    city, 
    state, 
    zipcode, 
    lat, 
    lng
    ) {
      if(!id || !name || !address || !country || !city || !state || !zipcode || !lat || !lng){
        throw new Error('The field is not empty')
      }
      this.id = id
      this.name = name
      this.address = address
      this.country = country
      this.city = city
      this.state = state
      this.zipcode = zipcode
      this.lat = lat
      this.lng = lng
  }
}

