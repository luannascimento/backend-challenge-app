'use strict'

const AWS = require('aws-sdk')
const CoworkingRepository = require('./repository/coworking.repository')

module.exports.list = async (event, context, callback) => {
  try {
    const cr = new CoworkingRepository(new AWS.DynamoDB.DocumentClient(), process.env.DYNAMODB_TABLE)
    const result = await cr.getAll()
    callback(null, {
      statusCode: 200,
      body: JSON.stringify(result)
    })
  } catch (error) {
    callback(null, {
      statusCode: 500,
      body: JSON.stringify({
        message: error.message
      })
    })
  }
}