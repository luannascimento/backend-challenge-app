'use strict'

const uuid = require('uuid')
const AWS = require('aws-sdk')
const validBody = require('./valid.body')
const Coworking = require('./coworking/coworking')
const CoworkingRepository = require('./repository/coworking.repository')

const required = ['name', 'address', 'country', 'city', 'state', 'zipcode', 'lat', 'lng']

module.exports.create = async (event, context, callback) => {
  try {
    const data = JSON.parse(event.body)
    
    if(!validBody(required, data)){
      callback(null, {
        statusCode: 400,
        body: JSON.stringify({
          message: `Bad Request Error`
        })
      })
    }

    const c = new Coworking(
      uuid.v4(), 
      data.name, 
      data.address, 
      data.country, 
      data.city, 
      data.state, 
      data.zipcode, 
      data.lat, 
      data.lng)

      const cr = new CoworkingRepository(new AWS.DynamoDB.DocumentClient(), process.env.DYNAMODB_TABLE)
      await cr.save(c)
      callback(null, {
        statusCode: 200,
        body: JSON.stringify({
          message: 'create coworking'
        })
      })
  } catch (error) {
    callback(null, {
      statusCode: 500,
      body: JSON.stringify({
        message: error.message
      })
    })
  }
}