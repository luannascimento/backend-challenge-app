module.exports = (required, body) => {
  let fields = []
  required.forEach(field => {
    if(!body[field] || body[field] === ''){
      fields.push(field)
    }
  })
  return (fields.length === 0)
}