const Faker = require('Faker')
const validBody = require('./../src/valid.body');

describe('Valid body function', () => {

  test('body ok', () => {
    const required = ['name', 'address']

    const body = {
      name: Faker.Name.firstName(), 
      address: Faker.Address.streetAddress()
    }
  
    expect(validBody(required, body)).toBe(true);
  });

  test('body not send', () => {
    const required = ['name', 'address', 'country', 'city', 'state', 'zipcode', 'lat', 'lng']
  
    expect(validBody(required, {})).toBe(false);
  });

  test('body field empty', () => {
    const required = ['name', 'address']

    const body = {
      name: Faker.Name.firstName(), 
      address: ''
    }
  
    expect(validBody(required, body)).toBe(false);
  });
})