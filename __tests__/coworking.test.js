
const uuid = require('uuid')
const Faker = require('Faker')
const Coworking = require('../src/coworking/coworking')

describe('class coworking', () => {

  test('instance class not fields', () => {
    expect(() => {
      new Coworking()
    }).toThrow(Error)
  })

  test('instance class', () => {
    new Coworking(
      uuid.v4(),
      Faker.Name.firstName(),
      Faker.Address.streetAddress(),
      Faker.Address.ukCountry(),
      Faker.Address.city(),
      Faker.Address.usState(),
      Faker.Address.zipCode(),
      Faker.Address.latitude(),
      Faker.Address.longitude())
    expect(true)
  })
})