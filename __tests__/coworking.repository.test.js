
const uuid = require('uuid')
const Faker = require('Faker')
const Coworking = require('../src/coworking/coworking')
const CoworkingRepository = require('../src/repository/coworking.repository')
const {DocumentClient} = require('aws-sdk/clients/dynamodb')

const isTest = process.env.JEST_WORKER_ID

const config = {
  convertEmptyValues: true,
  ...(isTest && {
    endpoint: 'localhost:8000',
    sslEnabled: false,
    region: 'local-env'
  })
}

const ddb = new DocumentClient(config)

describe('class coworking repository', () => {

  test('instance class not dynamodb', () => {
    expect(() => {
      new CoworkingRepository()
    }).toThrow(Error)
  })

  test('instance class', () => {
    new CoworkingRepository(ddb, 'coworkings')
    expect(true)
  })

  it('getCoworkingMinDistance', async (done) => {
    const cr = new CoworkingRepository(ddb, 'coworkings')

    let c = new Coworking(
      uuid.v4(),
      'Coworking São Paulo',
      'Does Not Matter, 123',
      'BR',
      'São Paulo',
      'SP',
      '01000000',
      '-23.533773',
      '-46.625290')

    await cr.save(c)

    c = new Coworking(
      uuid.v4(),
      'Coworking Rio de Janeiro',
      'Really, 345',
      'BR',
      'Rio de Janeiro',
      'RJ',
      '05400100',
      '-22.908333',
      '-43.196388')

    await cr.save(c)

    c = new Coworking(
      uuid.v4(),
      'Coworking New York',
      'Good Luck, 567',
      'USA',
      'New York City',
      'NY',
      '10001000',
      '40.730610',
      '-73.935242')

    await cr.save(c)

    c = new Coworking(
      uuid.v4(),
      'Coworking San Francisco',
      'Enjoy, 789',
      'USA',
      'San Francisco',
      'CA',
      '94110000',
      '37.733795',
      '-122.446747')

    await cr.save(c)

    let myLocation = {
      lat: -23.58412603264412,
      lng: -406.34033203125006
    }

    const result1 = await cr.getCoworkingMinDistance(myLocation)

    myLocation = {
      lat: 38.134556577054134,
      lng: -95.36132812500001
    }

    const result2 = await cr.getCoworkingMinDistance(myLocation)

    myLocation = {
      lat: 38.20365531807151,
      lng: -105.55664062500001
    }

    const result3 = await cr.getCoworkingMinDistance(myLocation)

    expect('Coworking São Paulo').toBe(result1.name)
    expect('Coworking New York').toBe(result2.name)
    expect('Coworking San Francisco').toBe(result3.name)
    done()
  })

  it('save', async (done) => {
    const c = new Coworking(
      uuid.v4(),
      Faker.Name.firstName(),
      Faker.Address.streetAddress(),
      Faker.Address.ukCountry(),
      Faker.Address.city(),
      Faker.Address.usState(),
      Faker.Address.zipCode(),
      Faker.Address.latitude(),
      Faker.Address.longitude())

    const cr = new CoworkingRepository(ddb, 'coworkings')
 
    try {
      await cr.save(c)
    } catch (error) {
      fail(error)
      done()
    }

    expect(true)
    done()
  })

  it('getAll', async (done) => {
    const cr = new CoworkingRepository(ddb, 'coworkings')

    try {
      await cr.getAll()
    } catch (error) {
      fail(error)
      done()
    }

    expect(true)
    done()
  })

})